<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <html lang="en">
<head>
  <title>Prism Utility</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</head>
<body>
 <div ><h2 align="center">Prism Utility</h2></div>	
<div class="container">
   
  <form:form class="form-horizontal" action="${pageContext.request.contextPath}/generateApplication" method="post" modelAttribute="form" enctype="multipart/form-data">
    <div class="form-group">
      <label   for="action">Action:</label>
     
        <form:select path="projectType" items="${projectType}" />
     
    </div>
    <div class="form-group">
      <label  for="input">Input Json:</label>
          
          
           <input type="file" name="file" /><br/><br/>
      
          
         
    </div>
    <div class="form-group">        
      
        <button type="submit"  class="btn btn-primary">Submit</button>
      
    </div>
    
  </form:form>
</div>

</body>
</html>
 