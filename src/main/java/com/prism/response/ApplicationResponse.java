package com.prism.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationResponse {
    private String successMessage;
    private String errorMessage;
    private String projectLocation;
    private String updatedJson;
}
