package com.prism.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class AccessResponse {
    private Integer id;
    private String username;
    private String name;
    private String active;
    private Date created_at;
    private Integer access_level;
}
