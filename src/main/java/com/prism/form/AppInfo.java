/*package com.prism.form;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Table(name = "AppInfo")
@javax.persistence.Entity
public class AppInfo implements Serializable {

 
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "AppId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer appId;

	@Column(name = "ArtifactId")
	private String artifactId;

	@Column(name = "GroupId")
	private String groupId;
     
	@Column(name="WinAppPath")
	private String winAppPath;
	
	@Column(name=" baseApplication")
	 private String baseApplication;
	
	@Column(name="RequestedJSON")
	private String requestedJSON;
     
	@Column(name="Version")
	private String Version;

}
*/