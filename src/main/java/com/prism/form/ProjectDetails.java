package com.prism.form;

import lombok.Getter;
import lombok.Setter;

public class ProjectDetails {
     
	@Getter @Setter private int pId;
    @Getter @Setter private String artifactId;
	@Getter @Setter private String groupId;
	@Getter @Setter private String packageName;
	@Getter @Setter private String appVersion;
	@Getter @Setter private String projectType;
	@Getter @Setter private String requestedJSON;	
}