package com.prism.form;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Top {
	
	@Getter @Setter @SerializedName("PrismServiceApplication") private Application application;

}
