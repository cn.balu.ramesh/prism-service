package com.prism.form;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;

public class PrismFieldNamingStrategy implements FieldNamingStrategy
{

	@Override
	public String translateName(java.lang.reflect.Field arg0) {
		   String fieldName =
		            FieldNamingPolicy.UPPER_CAMEL_CASE.translateName(arg0);
		        if (fieldName.startsWith("_"))
		        {
		            fieldName = fieldName.substring(1);
		        }
		        return fieldName;
	}
} 	
