 
package com.prism.form;

import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
 
import lombok.Getter;
import lombok.Setter;
 
@Getter
@Setter
public class Application {

    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("Name")
    @Expose
    @NotNull
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("BaseApplication")
    @Expose
    private String baseApplication;
    @SerializedName("Business")
    @Expose
    private String business;
    @SerializedName("ArtifactId")
    @Expose
    private String artifactId;
    @SerializedName("GroupId")
    @Expose
    private String groupId;
    
    @SerializedName("Version")
    @Expose
    private String version;
    
  
   
}
