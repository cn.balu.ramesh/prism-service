package com.prism.web;
 

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;
import org.springframework.core.Ordered;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.common.collect.ImmutableList;
import com.google.common.base.Predicate;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import javax.annotation.PostConstruct;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
@SpringBootApplication
@ComponentScan(basePackages = { "com.prism.controller" })
@EnableSwagger2
@Slf4j
public class PrismServiceApplication {

    private static final Logger log = LoggerFactory.getLogger(PrismServiceApplication.class);

   @Bean
   public Docket postsApi() {
      return new Docket(DocumentationType.SWAGGER_2)//.groupName("public-api")
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
            .paths(PathSelectors.any())
            .paths(postPaths()).build().useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, getCustomizedResponseMessages());
   }

   private Predicate<String> postPaths() {
      return or(regex("/.*"), regex("/.*"));
   }

   private ApiInfo apiInfo() {
      return new ApiInfoBuilder().title("Prism-service REST API")
            .description("Prism-service REST API reference for developers")
            .termsOfServiceUrl("http://prism.com")
            .contact(new Contact("Prism", "swagger-ui.html", "prism@gmail.com"))
            .license("PrismService License")
            .version("0.0.1").build();
   }
   
   private List<ResponseMessage> getCustomizedResponseMessages(){
        List<ResponseMessage> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseMessageBuilder().code(500).message("Server has crashed!!").responseModel(new ModelRef("Error")).build());
        responseMessages.add(new ResponseMessageBuilder().code(403).message("You shall not pass!!").build());
        return responseMessages;
    }

   public static void main(String[] args) {
   // SpringApplication.run(PrismServiceApplication.class, args);
      
    ConfigurableApplicationContext application = SpringApplication.run(PrismServiceApplication.class, args);
        Environment env = application.getEnvironment();
        try {
         log.info(
                 "\n-------------------------------------------------------------------------------------------\n\t"
                         + "Application '{}' is running! Access URLs:\n\t" + "Local\t\t: http://localhost:{}/\n\t"
                         + "External\t: http://{}:{}/\n\t" + "Doc\t\t: http://{}:{}/\n\t" + "Profile(s)\t: {}\n"
                         + "------------------------------------------------------------------------------------------",
                 env.getProperty("spring.application.name"), env.getProperty("server.port"),
                 InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"),
                 InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"), env.getActiveProfiles());
      } catch (UnknownHostException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
         
    }

/*
@Bean
	public WebMvcConfigurer configurer(){
log.info("*********************Coming to CorsConfiguration addCorsMappings ************************");

		return new WebMvcConfigurer(){
			@Override
			public void addCorsMappings(CorsRegistry registry) {
log.info("*********************INSIDE ************************");

				registry.addMapping("*")
               .allowedMethods("OPTIONS", "GET", "PUT", "POST", "DELETE")
               .allowedOrigins("*")
               .allowedHeaders("*");
;
			}
		};
	}

*/
// Fix the CORS errors
    @Bean
    public FilterRegistrationBean simpleCorsFilter() {  
log.info("*********************Coming to CorsConfiguration addCorsMappings ************************");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();  
        CorsConfiguration config = new CorsConfiguration();  
        config.setAllowCredentials(true); 
        // *** URL below needs to match the Vue client URL and port ***
        //config.setAllowedOrigins(Collections.singletonList("http://localhost:4000")); 
        config.setAllowedOrigins(ImmutableList.of("*"));
        config.setAllowedMethods(Collections.singletonList("*"));  
        config.setAllowedHeaders(Collections.singletonList("*"));  
        source.registerCorsConfiguration("/**", config);  
        FilterRegistrationBean bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);  
        return bean;  
    }   


   
}
