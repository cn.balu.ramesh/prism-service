package com.prism.constants;

import org.springframework.stereotype.Component;
@Component
public class Constants {

   // Web URL
   public static final String IN_FORM = "/inputForm";
   public static final String GET_RESPONSE = "/getResponse";

   // Rest URL
   public static final String URL_CREATE_APPLICATION = "http://localhost:8083/createApplication";
   public static final String URL_MIGRATE_APPLICATION = "http://localhost:8083/migrate";

   // Rest URL
   public static final String GET_MY_APPLICATIONS = "/getMyApplications";
   public static final String GET_MY_APPLICATIONS_TEST = "/getMyApplicationsTest";
   public static final String GET_NEW_APPLICATIONS = "/getNewApplications";
   public static final String IS_USER_EXISTIN_GITLAB = "/checkUserExist";
   public static final String REQUEST_ACCESS = "/requestAccess";
   public static final String REQUEST_ACCESS_SUBJECT = "Request Access";
   public static final String CREATE_NEW_APPLICATION = "/createNewApplication";
   public static final String COMMIT_APPLICATION = "/commitApplication";
   public static final String GENERATE_APPLICATION = "/generateApplication";


   public static final String GET_MY_APPLICATIONS_TXT = "Get My Applications";
   public static final String GET_NEW_APPLICATIONS_TXT = "Get New Applications";
   public static final String IS_USER_EXISTIN_GITLAB_TXT = "Check If User Exist";
   public static final String REQUEST_ACCESS_TXT = "Get Access to New Application";
   public static final String CREATE_NEW_APPLICATION_TXT = "Create a New Application";
   public static final String COMMIT_APPLICATION_TXT= "Commit a Application Changes";
   public static final String GENERATE_APPLICATION_TXT= "Generate Application";

   public static final String JSON_APPLICATON= "Json";
   public static final String JSON_DOT= ".json";
   public static final String MASTER= "master";
   public static final String JAVA_APPLICATON= "Java";
   public static final String SAVE_APPLICATION = "/saveApplication";
   public static final String SAVE_APPLICATION_TXT = "Save Application";
   public static final String GIT_LAB_URL = "ec2-18-217-132-245.us-east-2.compute.amazonaws.com";

/*
   // Values from application.properties

   @Value("${TEST_HOST_URL}") public static String TEST_HOST_URL ;
    
   @Value("${ADMIN_TOKEN}") public static String ADMIN_TOKEN ;
   @Value("${WINDOW_PROJECT_DIR}") public static String WINDOW_PROJECT_DIR ;
   @Value("${LINUX_PROJECT_DIR}") public static String LINUX_PROJECT_DIR ;
   @Value("${API_V4_PROJECTS}") public static String API_V4_PROJECTS ;
   @Value("${PROTECTED_BRANCHES_MASTER}") public static String PROTECTED_BRANCHES_MASTER ;
   @Value("${WIN_CMD}") public static String WIN_CMD ;
   @Value("${LINUX_SH}") public static String LINUX_SH ;
   @Value("${ACCESS_REQUESTS}") public static String ACCESS_REQUESTS ;
   @Value("${APPROVE}") public static String APPROVE ;
   @Value("${WIN_CMD_DIR}") public static String WIN_CMD_DIR ;
   @Value("${LINUX_CMD_DIR}") public static String LINUX_CMD_DIR ;
   @Value("${MASTER}") public static String MASTER ;
   @Value("${INITIAL_COMMIT}") public static String INITIAL_COMMIT ;
   @Value("${JSON}") public static String JSON ;
   @Value("${SLASH}") public static String SLASH ;
   @Value("${API}") public static String API ;
   @Value("${SUDO}") public static String SUDO ;
   @Value("${ERROR}") public static String ERROR ;
   @Value("${WINDOWS}") public static String WINDOWS ;
   
   
   @Value("${CONFLICT}") public static String CONFLICT ;
   @Value("${GIT_PUSH}") public static String GIT_PUSH ;
   @Value("${GIT_COMMIT}") public static String GIT_COMMIT ;
   @Value("${GIT_CLONE}") public static String GIT_CLONE ;
   
   @Value("${WINDOWS_BATCH_LOCATION}") public static String WINDOWS_BATCH_LOCATION ;
   @Value("${LINUX_BATCH_LOCATION}") public static String LINUX_BATCH_LOCATION ;
   
   @Value("${LINUX_PRJ_GENERATED_DIR}") public static String LINUX_PRJ_GENERATED_DIR ; 
   
*/






}