package com.prism.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class GitRequest {
    private Integer id;
    private String name;
    private String usename;
    private String state;
    private String avatar_url;
    private String web_url;
    private Date requested_at;

}
