package com.prism.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommitRequest {
    private Integer projectId;
    private User user;
    private String jsonString;
    private String commitMessage;
    private String branchName;
    private String projectName;
    private String authorName;
    private String authorEmail;
}