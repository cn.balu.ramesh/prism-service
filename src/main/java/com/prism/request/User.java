package com.prism.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {
    private Integer userId;
    private String userName;
    private String password;
    private String privateToken;
    private String hostUrl;
    private String emailAddress;
}
