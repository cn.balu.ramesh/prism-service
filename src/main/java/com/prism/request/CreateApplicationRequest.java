package com.prism.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateApplicationRequest {
        private Integer projectId;
        private String errorMsg;
}
