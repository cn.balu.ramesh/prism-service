package com.prism.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Application {

    private Integer projectId;
    private String applicationName;
    private String json;
}
