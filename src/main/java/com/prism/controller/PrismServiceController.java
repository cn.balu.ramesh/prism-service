package com.prism.controller;

import com.prism.constants.Constants;
import com.prism.request.Application;
import com.prism.request.CreateApplicationRequest;
import com.prism.request.GitRequest;
import com.prism.response.AccessResponse;
import com.prism.response.ApplicationResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONSerializer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApi.ApiVersion;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.RunnersApi;
import org.gitlab4j.api.models.*;
import org.gitlab4j.api.utils.AccessTokenUtils;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import static com.prism.constants.Constants.*;


@Api(tags = "Prism-service", value = "PrismServiceController")
@Slf4j
@RestController
public class PrismServiceController {

    @Value("${TEST_HOST_URL}")
    private String TEST_HOST_URL;

    @Value("${ADMIN_TOKEN}")
    private String ADMIN_TOKEN;
    @Value("${WINDOW_PROJECT_DIR}")
    private String WINDOW_PROJECT_DIR;
    @Value("${LINUX_PROJECT_DIR}")
    private String LINUX_PROJECT_DIR;
    @Value("${API_V4_PROJECTS}")
    private String API_V4_PROJECTS;
    @Value("${PROTECTED_BRANCHES_MASTER}")
    private String PROTECTED_BRANCHES_MASTER;
    @Value("${WIN_CMD}")
    private String WIN_CMD;
    @Value("${LINUX_SH}")
    private String LINUX_SH;
    @Value("${ACCESS_REQUESTS}")
    private String ACCESS_REQUESTS;
    @Value("${APPROVE}")
    private String APPROVE;
    @Value("${WIN_CMD_DIR}")
    private String WIN_CMD_DIR;
    @Value("${LINUX_CMD_DIR}")
    private String LINUX_CMD_DIR;
    @Value("${MASTER}")
    private String MASTER;
    @Value("${INITIAL_COMMIT}")
    private String INITIAL_COMMIT;
    @Value("${JSON}")
    private String JSON;
    @Value("${SLASH}")
    private String SLASH;
    @Value("${API}")
    private String API;
    @Value("${SUDO}")
    private String SUDO;
    @Value("${ERROR}")
    private String ERROR;
    @Value("${WINDOWS}")
    private String WINDOWS;

    @Value("${CONFLICT}")
    private String CONFLICT;
    @Value("${GIT_PUSH}")
    private String GIT_PUSH;
    @Value("${GIT_COMMIT}")
    private String GIT_COMMIT;
    @Value("${GIT_CLONE}")
    private String GIT_CLONE;

    @Value("${WINDOWS_BATCH_LOCATION}")
    private String WINDOWS_BATCH_LOCATION;
    @Value("${LINUX_BATCH_LOCATION}")
    private String LINUX_BATCH_LOCATION;

    @Value("${LINUX_PRJ_GENERATED_DIR}")
    private String LINUX_PRJ_GENERATED_DIR;

    @Value("${WINDOWS_PRJ_GENERATED_DIR}")
    private String WINDOWS_PRJ_GENERATED_DIR;

    @Value("${GIT_LAB_URL}")
    private String GIT_LAB_URL;

    @Value("${ADMIN_USER_NAME}")
    private String ADMIN_USER_NAME;
    @Value("${ADMIN_USER_PASSWORD}")
    private String ADMIN_USER_PASSWORD;

    @Value("${PROTOCOL}")
    private String PROTOCOL;

    @Value("${VERSION}")
    private String projectversion;


    @Value("${spring.mail.username}")
    private String from;

    private String DEVELOPMENT_BRANCH_NAME="development";
    private String PRISM_BRANCH_NAME="prism";

    private static String os = SystemUtils.OS_NAME;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    protected Configuration freemarker;


    @PostConstruct
    private void postConstruct() {
        System.out.printf("GIT_LAB_URL= %s, ADMIN_USER_NAME= %s%n", GIT_LAB_URL, ADMIN_USER_NAME);
    }


    @PostMapping(Constants.GET_MY_APPLICATIONS)
    @ApiOperation(value = Constants.GET_MY_APPLICATIONS_TXT)
    public List<Application> getMyApplications(
            @ApiParam(value = "Enter the UserName ", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the User Access Token", required = true) @Valid @RequestHeader("privateToken") String privateToken)
            throws GitLabApiException {
        log.info("Get My Application ::: " + userName);
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, privateToken);
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        List<Project> maintainerProjects = gitLabAdminApi.getProjectApi().getMemberProjects();
        List<Project> projectList = maintainerProjects.stream().filter(project -> project.getName().endsWith(JSON_APPLICATON)).collect(Collectors.toList());
        List<String> projectNameList = projectList.stream().map(Project::getName).collect(Collectors.toList());
        List<Project> memberProjects = gitLabApi.getProjectApi().getProjects(false, Visibility.PRIVATE, org.gitlab4j.api.Constants.ProjectOrderBy.NAME, null, null, true, false, true, false, true);
        List<Project> filteredProjects = memberProjects.stream().filter(project -> projectNameList.contains(project.getName())).collect(Collectors.toList());
        List<Application> applications = convertProjectToApplication(filteredProjects, userName, false, privateToken);
        return applications;

    }
    @PostMapping(Constants.GET_MY_APPLICATIONS_TEST)
    @ApiOperation(value = Constants.GET_MY_APPLICATIONS_TXT)
    public List<Application> getMyApplicationsTest(
            @ApiParam(value = "Enter the UserName ", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the User Access Token", required = true) @Valid @RequestHeader("privateToken") String privateToken)
            throws GitLabApiException {
        log.info("Get My Application ::: " + userName);
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, privateToken);
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        List<Project> maintainerProjects = gitLabAdminApi.getProjectApi().getMemberProjects();
        List<Project> projectList = maintainerProjects.stream().filter(project -> project.getName().endsWith(JSON_APPLICATON)).collect(Collectors.toList());
        List<String> projectNameList = projectList.stream().map(Project::getName).collect(Collectors.toList());
        List<Project> memberProjects = gitLabApi.getProjectApi().getProjects(false, Visibility.PRIVATE, org.gitlab4j.api.Constants.ProjectOrderBy.NAME, null, null, true, false, true, false, true);
        List<Project> filteredProjects = memberProjects.stream().filter(project -> projectNameList.contains(project.getName())).collect(Collectors.toList());
        List<Application> applications = convertProjectToApplication1(filteredProjects, userName, false, privateToken);
        return applications;

    }


    @PostMapping(Constants.SAVE_APPLICATION)
    @ApiOperation(value = Constants.SAVE_APPLICATION_TXT)
    public String saveApplication(
            @ApiParam(value = "Enter the UserName ", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the Project name ", required = true) @Valid @RequestHeader("projectName") String projectName,
            @ApiParam(value = "Enter the Json ", required = true) @Valid @RequestBody String json)
            throws GitLabApiException, IOException {

        log.info("Save the saveApplication ::: " + projectName);
        String os = SystemUtils.OS_NAME;
        String projectBasePath = os.toLowerCase().startsWith(WINDOWS) ? MessageFormat.format(WINDOW_PROJECT_DIR, userName) : MessageFormat.format(LINUX_PROJECT_DIR, userName);
        System.out.println("Project Base Path >>>>>>>>>" + projectBasePath);
        String applicationName = StringUtils.deleteWhitespace(projectName) + JSON_APPLICATON;
        Path path = Paths.get(projectBasePath  + SLASH + applicationName + SLASH
                + projectName.toLowerCase() + "json" + JSON);
        FileChannel.open(path, StandardOpenOption.WRITE).truncate(0).close();

        try (FileWriter writer = new FileWriter(path.toString()); BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(json);

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
            return e.getMessage();
        }
        return "Application Saved SuccessFully";

    }


    @PostMapping(Constants.GET_NEW_APPLICATIONS)
    @ApiOperation(value = Constants.GET_NEW_APPLICATIONS_TXT)
    public List<Application> getNewApplications(
            @ApiParam(value = "Enter the UserName ", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the User Access Token", required = true) @Valid @RequestHeader("privateToken") String privateToken)
            throws GitLabApiException {
        log.info("Get All New Application ::: " + userName);
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, privateToken);
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        List<Project> maintainerProjects = gitLabAdminApi.getProjectApi().getMemberProjects();

        List<Project> projectList = maintainerProjects.stream().filter(project -> project.getName().endsWith(JSON_APPLICATON)).collect(Collectors.toList());
        List<Project> memberProjects = gitLabApi.getProjectApi().getProjects(false, Visibility.PRIVATE, org.gitlab4j.api.Constants.ProjectOrderBy.NAME, null, null, true, false, true, false, true);

        List<String> projectNameList = memberProjects.stream().map(Project::getName).collect(Collectors.toList());

        List<Project> projectList1 = projectList.stream()
                .filter(project -> !projectNameList.contains(project.getName())).collect(Collectors.toList());
        List<Application> applications = convertProjectToApplication(projectList1, userName, true, privateToken);
        return applications;
    }

    @PostMapping(Constants.IS_USER_EXISTIN_GITLAB)
    @ApiOperation(value = Constants.IS_USER_EXISTIN_GITLAB_TXT)
    public String isUserExistInGitlab(
            @ApiParam(value = "Enter the UserName", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the Password", required = true) @Valid @RequestHeader("password") String password)
            throws GitLabApiException {
        log.info("Check If User Exist ::: " + userName);
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        System.out.println("gitLabAdminApi :::: " + gitLabAdminApi);
        org.gitlab4j.api.models.User user = gitLabAdminApi.getUserApi().getUser(userName);
        System.out.println("GetName :::: " + user);
        boolean isExist = user != null && user.getName() != null;
        if (isExist) {
            return getMyPersonalToken(userName, password);
        }
        return null;
    }


    @PostMapping(Constants.REQUEST_ACCESS)
    @ApiOperation(value = Constants.REQUEST_ACCESS_TXT)
    public AccessResponse requestAccessApplication(
            @ApiParam(value = "Enter the User Token", required = true) @Valid @RequestHeader("privateToken") String privateToken,
            @ApiParam(value = "Enter the Project ID", required = true) @Valid @RequestHeader("projectId") Integer projectId)
            throws GitLabApiException {
        log.info("Get Access to New Application ::: " + privateToken);
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        Project project = gitLabAdminApi.getProjectApi().getProject(projectId);
        User adminUser = gitLabAdminApi.getUserApi().getCurrentUser();
        GitLabApi usergit = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, privateToken);
        User user = usergit.getUserApi().getCurrentUser();
        try {
            if (adminUser!=null){
                sendMail(adminUser.getEmail(), adminUser.getName(),user.getName(), project);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        AccessResponse accessResponse = new AccessResponse();
        accessResponse.setCreated_at(new Date());
        return accessResponse;

    }

    @PostMapping(Constants.CREATE_NEW_APPLICATION)
    @ApiOperation(value = Constants.CREATE_NEW_APPLICATION_TXT, response = String.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 401, message = "not authorized!"),
            @ApiResponse(code = 403, message = "forbidden!!!"),
            @ApiResponse(code = 404, message = "not found!!!")})
    public CreateApplicationRequest createNewApplication(
            @ApiParam(value = "Enter the Username", required = true) @Valid @RequestHeader String userName,
            @ApiParam(value = "Enter the User Private Token ", required = true) @Valid @RequestHeader String privateToken,
            @ApiParam(value = "Enter the Application Name", required = true) @Valid @RequestHeader String applicationName,
            @ApiParam(value = "Enter the JSON String", required = true) @Valid @RequestBody String json) throws ExecutionException, InterruptedException, GitLabApiException, IOException {

        log.info("Create New Application For Json::: " + applicationName);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("PRIVATE-TOKEN", ADMIN_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);

        String projectBasePath = os.toLowerCase().startsWith(WINDOWS) ? WINDOWS_PRJ_GENERATED_DIR:LINUX_PRJ_GENERATED_DIR;
        System.out.println(">>>>>>>>>>>> base Json proj " +projectBasePath);
        CreateApplicationRequest createApplicationRequest = new CreateApplicationRequest();
        Integer userId;
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        try {
            userId = gitLabAdminApi.getUserApi().getUser(userName).getId();
        } catch (GitLabApiException e) {
            createApplicationRequest.setErrorMsg(e.getMessage());
            return createApplicationRequest;
        }

        if (!new File(projectBasePath).exists()){
            new File(projectBasePath).mkdirs();
        }

        ApplicationResponse applicationResponse = generateApplicationByJson(projectBasePath, json);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        Callable<String> javaTask = () -> createJavaProject(json, applicationResponse.getProjectLocation(), gitLabAdminApi, privateToken, userId, headers,userName);
        Callable<CreateApplicationRequest> jsonTask = () -> createJsonProject(applicationName, gitLabAdminApi, applicationResponse.getUpdatedJson(), userName, userId, privateToken, headers);
        Future<CreateApplicationRequest> createApplicationRequestFuture = executor.submit(jsonTask);
        executor.submit(javaTask);

        CreateApplicationRequest createApplicationRequest1 = createApplicationRequestFuture.get();

        return createApplicationRequest1;


    }

    @PostMapping(Constants.COMMIT_APPLICATION)
    @ApiOperation(value = Constants.COMMIT_APPLICATION_TXT)
    public String commitApplication(
            @ApiParam(value = "Enter the User Name", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the Password", required = true) @Valid @RequestHeader("password") String password,
            @ApiParam(value = "Enter the User Access Token", required = true) @Valid @RequestHeader("privateToken") String privateToken,
            @ApiParam(value = "Enter the Project Id", required = true) @Valid @RequestHeader("projectId") String projectId,
            @ApiParam(value = "Enter the Commit Message", required = true) @Valid @RequestParam(value = "commitMessage", required = false) String commitMessage,
            @ApiParam(value = "Enter the JSON String", required = true) @Valid @RequestBody String jsonString)
            throws IOException, GitLabApiException {

        String os = SystemUtils.OS_NAME;
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, privateToken);
        ApplicationResponse applicationResponse = null;

        // TODO: 6/4/2019 Todo Application
        try {
            String projectBasePath = os.toLowerCase().startsWith(WINDOWS) ? WINDOWS_PRJ_GENERATED_DIR:LINUX_PRJ_GENERATED_DIR;
            String shellScriptLocation = os.toLowerCase().startsWith("windows") ? WINDOWS_BATCH_LOCATION : LINUX_BATCH_LOCATION;
            String commandName = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD : LINUX_SH;
            String cmdDirectory = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD_DIR : LINUX_CMD_DIR;

            net.sf.json.JSONObject jsonApp = (net.sf.json.JSONObject) JSONSerializer.toJSON(jsonString);
            net.sf.json.JSONObject application = (net.sf.json.JSONObject) jsonApp.get("Application");
            String javaApplicationName = (String) application.get("Name");
            log.info("Project Name >>>>>>>>>>>>" + javaApplicationName);
            javaApplicationName = javaApplicationName + JAVA_APPLICATON;
            List<Project> projectList = gitLabApi.getProjectApi().getProjects(false, Visibility.PRIVATE, org.gitlab4j.api.Constants.ProjectOrderBy.NAME, null, null, true, false, true, false, true);
            String finalJavaApplicationName = javaApplicationName;
            Project updatedProject=null;

            if (projectList!=null){
                updatedProject = projectList.stream().filter(project1 -> StringUtils.deleteWhitespace(finalJavaApplicationName).equalsIgnoreCase(project1.getName())).findFirst().orElseGet(null);
                log.info("Commit Project Name >>>>>>>>>>>>" + updatedProject!=null?updatedProject.getName():"");
            }


            if (updatedProject != null) {
                applicationResponse = generateApplicationByJson(projectBasePath, jsonString);
                String projectName = StringUtils.substringAfterLast(applicationResponse.getProjectLocation(), File.separator);
                projectName = StringUtils.substringBeforeLast(projectName, ".");
                projectName = StringUtils.deleteWhitespace(projectName);
                log.info("&&&&&&&&&&&&&&&&& Project Name >>>>>>>>>>>>" + projectName);
                StringBuilder builder = new StringBuilder();
                builder.append(PROTOCOL).append(userName).append(":").append(encodeURLComponent(password)).append("@" + GIT_LAB_URL+ "/")
                        .append(ADMIN_USER_NAME).append("/").append(projectName).append(".git");
                log.info("StringBuilder Project Name >>>>>>>>>>>>" + builder.toString());
                String pullResult = getcommitCommands(userName, projectName, " git pull ", projectBasePath);
                if (StringUtils.containsOnly(pullResult, ERROR)) {
                    return pullResult;
                }
                createOrUpdateGit(commandName, cmdDirectory, shellScriptLocation, projectBasePath + "/" + projectName, builder.toString(), commitMessage , projectName );
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Project Id >>>>>>>>>> "+projectId);

        Project project = gitLabApi.getProjectApi().getProject(projectId);

        log.info("Commit Application ::: " + project.getName());

        String projectDirectory = os.toLowerCase().startsWith(WINDOWS) ? MessageFormat.format(WINDOW_PROJECT_DIR, userName)+ SLASH
                : MessageFormat.format(LINUX_PROJECT_DIR, userName)+ SLASH;
        log.info("Commit Application 222222222::: " + projectDirectory);
        Path path = Paths.get(projectDirectory  + project.getName() + SLASH
                + project.getName().toLowerCase() + JSON);

        log.info("Commit Application 333333333::: " + projectDirectory  + project.getName() + SLASH + project.getName().toLowerCase() + JSON);
        FileChannel.open(path, StandardOpenOption.WRITE).truncate(0).close();

        try (FileWriter writer = new FileWriter(path.toString());
             BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(applicationResponse.getUpdatedJson());

        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        System.out.println("getHttpUrlToRepo >>>>>>>>>> " + project.getHttpUrlToRepo());

        String result = getcommitCommands(userName, project.getName(), " git pull ", projectDirectory);
        if (StringUtils.containsAny(result.toLowerCase(), ERROR, CONFLICT)) {
            return result;
        }
        String commitResult = getcommitCommands(userName, project.getName(),
                GIT_COMMIT + " -am \""+ " "+userName +": " + StringUtils.defaultIfBlank(commitMessage, "") + "\"", null);

        if (StringUtils.containsOnly(result, ERROR)) {
            return commitResult;
        }
        String gitHttpUrl = project.getHttpUrlToRepo();
        String userNamePassword = userName + ":" + encodeURLComponent(password) + "@" + GIT_LAB_URL;
        String baseUrl = StringUtils.replace(gitHttpUrl, GIT_LAB_URL, userNamePassword);
        String pushResult = getcommitCommands(userName, project.getName(), GIT_PUSH + "  " + baseUrl, null);
        if (StringUtils.containsOnly(result, ERROR)) {
            return pushResult;
        }

        StringBuilder stringBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(path)) {
            stream.forEach(json -> {
                stringBuilder.append(json);
            });
        }
        if (applicationResponse.getProjectLocation() != null) {
            FileUtils.deleteQuietly(new File(applicationResponse.getProjectLocation()));
        }


        return stringBuilder.toString();
    }


    @PostMapping(Constants.GENERATE_APPLICATION)
    @ApiOperation(value = Constants.GENERATE_APPLICATION_TXT)
    public String generateApplication(
            @ApiParam(value = "Enter the User Name", required = true) @Valid @RequestHeader("userName") String userName,
            @ApiParam(value = "Enter the Password", required = false) @Valid @RequestHeader("password") String password,
            @ApiParam(value = "Enter the Json String ", required = true) @Valid @RequestBody String jsonApplicationString, HttpServletResponse response) throws IOException {
        log.info("Generate Application ::: " + jsonApplicationString);


        String os = SystemUtils.OS_NAME;
        ApplicationResponse applicationResponse = null;
        String fileName = null;
        String errorResponse = null;
        try {
            ClassPathResource resource = new ClassPathResource(
                    org.apache.commons.lang3.StringUtils.startsWithAny(os, "windows", "Windows")
                            || org.apache.commons.lang3.StringUtils.equalsIgnoreCase(os, "windows")
                            ? "json-schema-draft-06.json"
                            : "classpath:json-schema-draft-06.json");
            InputStream in = resource.getInputStream();
            JSONObject jsonSchema = new JSONObject(new JSONTokener(in));

            JSONObject jsonSubject = new JSONObject(new JSONTokener(jsonApplicationString));
            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonSubject);

            String projectBasePath = os.toLowerCase().startsWith(WINDOWS) ? WINDOWS_PRJ_GENERATED_DIR:LINUX_PRJ_GENERATED_DIR;
            applicationResponse = generateApplicationByJson(projectBasePath, jsonApplicationString);
            fileName = applicationResponse.getProjectLocation();
            response.setContentType("application/octet-stream");
            String downloadFileName = StringUtils.substringAfterLast(applicationResponse.getProjectLocation(), File.separator);
            response.addHeader("Content-Disposition", "attachment; filename=" + downloadFileName);

            try {
                Files.copy(Paths.get(fileName), response.getOutputStream());
                response.getOutputStream().flush();
                if (applicationResponse.getProjectLocation() != null) {
                    FileUtils.deleteQuietly(new File(applicationResponse.getProjectLocation()));
                }

            } catch (Exception ex) {
                if (ex instanceof ValidationException) {
                    List<String> errors = new ArrayList<>();
                    errors.add("JSON File Error");
                    errors.add("********************************************************************************************");
                    var error = ((ValidationException) ex).getCausingExceptions().stream()
                            .flatMap(e1 -> e1.getAllMessages().stream()).collect(Collectors.toList());
                    errors.add(String.join("", error));
                    var allError = ((ValidationException) ex).getAllMessages();
                    errors.add(String.join("", allError));
                    errorResponse = String.join("", errors);

                }
            }
        } catch (Exception e) {
            if (e instanceof ValidationException) {
                List<String> errors = new ArrayList<>();
                errors.add("JSON File Error");
                errors.add(
                        "********************************************************************************************");
                var error = ((ValidationException) e).getCausingExceptions().stream()
                        .flatMap(e1 -> e1.getAllMessages().stream()).collect(Collectors.toList());
                errors.add(String.join("", error));
                var allError = ((ValidationException) e).getAllMessages();
                errors.add(String.join("", allError));
                errorResponse = String.join("", errors);

            } else {
                errorResponse = e.getMessage();
            }

        }
        if (errorResponse != null) {
            return errorResponse;
        }
        return fileName;
    }

    @PostMapping("/removeApplication")
    @ApiOperation(value = "Delete Application by Project Id from gitlab Prism Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deleted Application from gitlab Prism Admin", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Project Id not found")
    })
    public String removeApplication(  @ApiParam(value = "Enter the Project Id ", required = true) @Valid @RequestHeader("projectId") Integer projectId,
                                    @ApiParam(value = "Enter the User Name ", required = true) @Valid @RequestHeader("userName") String userName)
            throws GitLabApiException {
        log.info("Get All New Application ::: ");
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        org.gitlab4j.api.models.User user = gitLabApi.getUserApi().getUser(userName);
        if (user!=null){
            gitLabApi.getProjectApi().removeMember(projectId,user.getId());
        }


        //removeApplication(token, projectId ,userId);
        return "Deleting ProjectId :::: " + projectId;
    }


    @PostMapping("/getApplication")
    @ApiOperation(value = "Get Application by Project Id ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get Application ", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Project Id not found")
    })
    public String getApplication( @ApiParam(value = "Enter the Project Id ", required = true) @Valid @RequestHeader("projectId") Integer projectId,
                                      @ApiParam(value = "Enter the User Name ", required = true) @Valid @RequestHeader("userName") String userName,
                                       @ApiParam(value = "Enter the token of User ", required = true) @Valid @RequestHeader("token") String token)
            throws GitLabApiException, IOException {
        Project project = new Project();
        project.setId(projectId);
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, token);
        Project project1  = gitLabApi.getProjectApi().getProject(project);
        return getGitProject(userName,project1,token,null);
    }


    @PostMapping("/removeApplications")
    @ApiOperation(value = "DeleteAll Application from gitlab Prism Admin", response = String.class, hidden = true)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "DeleteAll Application from gitlab Prism Admin", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Project Id not found")
    })
    public String removeApplications()
            throws GitLabApiException {
        log.info("Get All New Application ::: ");
        GitLabApi gitLabApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        List<Project> projects = gitLabApi.getProjectApi().getOwnedProjects();
       // List<Integer> projectIds = List.of( 12957956,12957954,12958021,12958020);
        if (projects != null) {
            System.out.println("Inside If::::::::::");
            for (Project prj : projects) {
               /* if (projectIds.contains(prj.getId())){
                    System.out.println("$$$$$$$$ " +prj.getId());
                    continue;
                }*/
                System.out.println("Deleting ProjectId :::: " + prj.getId() + " ::: Project Name ::: " + prj.getName());
                //removeApplication(ADMIN_TOKEN, prj.getId());
removeApplication(prj.getId(), ADMIN_TOKEN);
            }

        }
        return "";
    }

    private String getGitProject(String userName, Project project, String privateToken,String projectDirectory) throws IOException {
        log.info(" Get GetGitProject ::: ");
        String os = SystemUtils.OS_NAME;
        String commandName = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD : LINUX_SH;
        String cmdDirectory = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD_DIR : LINUX_CMD_DIR;

        if (projectDirectory==null){
            projectDirectory = os.toLowerCase().startsWith(WINDOWS) ? MessageFormat.format(WINDOW_PROJECT_DIR, userName) + SLASH
                    : MessageFormat.format(LINUX_PROJECT_DIR, userName) + SLASH;
        }
        log.info(" Get projectDirectory ::: " + projectDirectory);
        File file = new File(projectDirectory);
        if (!file.exists()) {
            file.mkdirs();
        }
        File projdirectory = new File(projectDirectory + project.getName());

        if (!StringUtils.startsWithAny(os.toLowerCase(), WINDOWS)) {
            providePermission(userName);
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(PROTOCOL).append(userName).append(":").append(encodeURLComponent(privateToken)).append("@" + GIT_LAB_URL + "/")
                .append(ADMIN_USER_NAME).append("/").append(project.getName()).append(".git");
        log.info(" Get projectDirectory ::: stringBuilder ::::: " + stringBuilder.toString());
        if (!projdirectory.exists()) {
            String[] templateCommands = {commandName, cmdDirectory,
                    "cd " + projectDirectory + " && " + GIT_CLONE + " " + stringBuilder.toString()};

            try {
                ProcessBuilder builder = new ProcessBuilder(templateCommands);
                builder.redirectErrorStream(true);
                Process p = builder.start();
                BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                do {
                    line = r.readLine();
                    System.out.println("$$$$$$$$$$$$$ " +line);
                } while (line != null);

            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        System.out.println("%%%%%%%%%%%%%%%%%% " +projectDirectory  + project.getName().toLowerCase() + SLASH
                + project.getName().toLowerCase() + JSON);


        if (new File(projectDirectory  + project.getName() + SLASH  + project.getName().toLowerCase() + JSON).exists()) {

            FileReader fileReader = new FileReader(projectDirectory  + project.getName() + SLASH
                    + project.getName().toLowerCase() + JSON);
            BufferedReader br = new BufferedReader(fileReader);
            String line1;
            StringBuilder builder = new StringBuilder();
            while ((line1 = br.readLine()) != null) {
                builder.append(line1);
            }
            br.close();
            String processStr = builder.toString();
            net.sf.json.JSONObject jsonApp = (net.sf.json.JSONObject) JSONSerializer.toJSON(processStr);
            net.sf.json.JSONObject application = (net.sf.json.JSONObject) jsonApp.get("Application");
            String version = (String) application.get("Version");
            if (StringUtils.isBlank(version) || !StringUtils.equals(version,projectversion)){
                return  migrate(processStr);
            }

            return processStr;
        }

        return null;
    }

    private List<Application> convertProjectToApplication(List<Project> projects, String userName,
                                                          boolean isNewApplicationRequest, String privateToken) throws GitLabApiException {


        log.info("::::: Generate convertProjectToApplication ::: " + projects.size());
        List<Application> applicationList = new ArrayList<>();
        for (Project project : projects) {
            log.info("Project >>>>>>>>>"+project.getName());
            if (!project.getName().endsWith(JSON_APPLICATON)) {
                continue;
            }
            try {
                Application application = new Application();
                application.setProjectId(project.getId());
                application.setApplicationName(project.getName());
                if (!isNewApplicationRequest) {
                    try {
                        String json = getGitProject(userName, project, privateToken,null);

                        application.setJson(json);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                applicationList.add(application);

            } catch (Exception e) {
                // log.info("skipping" + project.getName());
            }
        }
        return applicationList;


    }
 private List<Application> convertProjectToApplication1(List<Project> projects, String userName,
                                                          boolean isNewApplicationRequest, String privateToken) throws GitLabApiException {


        log.info("::::: Generate convertProjectToApplication ::: " + projects.size());
        List<Application> applicationList = new ArrayList<>();
        for (Project project : projects) {
            log.info("Project >>>>>>>>>"+project.getName());
            if (!project.getName().endsWith(JSON_APPLICATON)) {
                continue;
            }
            try {
                Application application = new Application();
                application.setProjectId(project.getId());
                application.setApplicationName(project.getName());
                applicationList.add(application);

            } catch (Exception e) {
                // log.info("skipping" + project.getName());
            }
        }
        return applicationList;


    }

    private String getMyPersonalToken(String userName, String password) throws GitLabApiException {
        String tokenName = userName + "-" + getRandomInt(10000);
        String accessToken = AccessTokenUtils.createPersonalAccessToken(StringUtils.defaultIfBlank(null, TEST_HOST_URL),
                userName, password, tokenName, Arrays.asList(API, SUDO));
        return accessToken;
    }

    private String getcommitCommands(String userName, String projectName, String command, String projectDirectory) {

        log.info(userName + " Generate getcommitCommands ::: " + projectName + " :::projectDirectory:: " + projectDirectory);
        String os = SystemUtils.OS_NAME;
        String commandName = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD : LINUX_SH;
        String cmdDirectory = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD_DIR : LINUX_CMD_DIR;
        if (projectDirectory == null) {
            projectDirectory = os.toLowerCase().startsWith(WINDOWS) ? MessageFormat.format(WINDOW_PROJECT_DIR, userName)
                    : MessageFormat.format(LINUX_PROJECT_DIR, userName);
        }


        String[] templateCommands = {commandName, cmdDirectory,
                "cd " + projectDirectory + SLASH + projectName + "  && " + command};
        StringBuilder stringBuilder = new StringBuilder();
        try {
            ProcessBuilder builder = new ProcessBuilder(templateCommands);
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            do {
                line = r.readLine();
                stringBuilder.append(line);
                System.out.println(line);

            } while (line != null);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return stringBuilder.toString();
    }


    public void providePermission(String userName) {

        try {
            Process process = Runtime.getRuntime().exec("sudo chmod -R 777 " + MessageFormat.format(LINUX_PROJECT_DIR, userName));

            StringBuilder output = new StringBuilder();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    private AccessResponse requestAccess(String privateToken, Integer projectId) {
        log.info("Get Access to New Application ::: " + privateToken);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("PRIVATE-TOKEN", privateToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        RestTemplateBuilder restTemplate = new RestTemplateBuilder();
        String serviceUrl = StringUtils.defaultIfBlank(null, TEST_HOST_URL) + API_V4_PROJECTS + projectId + ACCESS_REQUESTS;
        ResponseEntity<AccessResponse> response1 = restTemplate.build().exchange(serviceUrl, HttpMethod.POST, requestEntity,
                AccessResponse.class);
        AccessResponse accessResponse = response1.getBody();
        return accessResponse;

    }


    private AccessResponse requestEnableGitLabRunnner(String privateToken, Integer projectId) {
        log.info("Get Access to New Application ::: " + privateToken);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("PRIVATE-TOKEN", privateToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        RestTemplateBuilder restTemplate = new RestTemplateBuilder();
        String serviceUrl = StringUtils.defaultIfBlank(null, TEST_HOST_URL) + API_V4_PROJECTS + projectId + ACCESS_REQUESTS;
        ResponseEntity<AccessResponse> response1 = restTemplate.build().exchange(serviceUrl, HttpMethod.POST, requestEntity,
                AccessResponse.class);
        AccessResponse accessResponse = response1.getBody();
        return accessResponse;

    }

    private void approveAccess(Integer projectId, Integer userId) throws GitLabApiException {
        log.info("Get Approve Access to New Application ::: " + userId);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("PRIVATE-TOKEN", ADMIN_TOKEN);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        RestTemplateBuilder restTemplate = new RestTemplateBuilder();
        String serviceUrl = StringUtils.defaultIfBlank(null, TEST_HOST_URL) + API_V4_PROJECTS + projectId
                + ACCESS_REQUESTS + userId + APPROVE;

        System.out.println("approveAccess >>>>>>>>>>>>>>> " + serviceUrl);
        ResponseEntity<GitRequest> response1 = restTemplate.build().exchange(serviceUrl, HttpMethod.PUT, requestEntity,
                GitRequest.class);

    }

    public static final int getRandomInt(int maxValue) {
        return ((int) (Math.random() * maxValue + 1));
    }


    private static void createOrUpdateGit(String commandName, String command, String shellScriptLocation,
                                          String projectLocation, String gitURL, String comments , String prjName) {
        log.info(" Generate createOrUpdateGit command ::: " + command + " :::projectLocation:: " + projectLocation);
        log.info("Command :::: " + commandName + " Generate createOrUpdateGit gitURL ::: " + gitURL + " :::comments:: " + comments);
        try {
            System.out.println("THE BASE PATH IS ******************" + projectLocation);

            System.out.println("THE gitURL IS ******************" + gitURL);
            String[] cmd = {commandName, command,
                    shellScriptLocation + " " + projectLocation + " " + gitURL + " " + comments + " " + prjName };
            System.out.println("BASH CMD>>>>>> " + Arrays.toString(cmd));
            ProcessBuilder pb = new ProcessBuilder(cmd);
            pb.redirectErrorStream(true);
            Process p = pb.start();
	        System.out.println("*************************************** ");

             
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            int exitCode = p.waitFor();
            System.out.println("\nExited with error code : " + exitCode);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void requestApproveAccess(String privateToken, Integer projectId, Integer userId) throws GitLabApiException {
        requestAccess(privateToken, projectId);
        System.out.println("UserId :::::: " + userId);
        approveAccess(projectId, userId);
        System.out.println(projectId + " getApproveAccess :::::: userId ::: " + userId);
    }

    private String migrate(String jsonString){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
            RestTemplateBuilder restTemplate = new RestTemplateBuilder();
            ResponseEntity<String> response1 = restTemplate.build().exchange(URL_MIGRATE_APPLICATION,
                    HttpMethod.POST, entity, String.class);
            restTemplate.build();
            return response1.getBody();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private ApplicationResponse generateApplicationByJson(String projectBasePath, String jsonApplicationString) {
        log.info("Generator the project");
        log.info("Generator the project");
        ApplicationResponse applicationResponse = new ApplicationResponse();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("projectPath", projectBasePath);
            HttpEntity<String> entity = new HttpEntity<String>(jsonApplicationString, headers);
            RestTemplateBuilder restTemplate = new RestTemplateBuilder();

            ResponseEntity<ApplicationResponse> response1 = restTemplate.build().exchange(Constants.URL_CREATE_APPLICATION,
                    HttpMethod.POST, entity, ApplicationResponse.class);
            restTemplate.build();
            applicationResponse = response1.getBody();


        } catch (Exception e) {
            e.printStackTrace();

            applicationResponse.setErrorMessage(e.getMessage());

            return applicationResponse;
        }
        return applicationResponse;

    }


    private AccessResponse removeApplication1(String privateToken, Integer projectId,Integer userId) {
        log.info("Get Access to New Application ::: " + privateToken);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("PRIVATE-TOKEN", privateToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        RestTemplateBuilder restTemplate = new RestTemplateBuilder();
        String serviceUrl = StringUtils.defaultIfBlank(null, TEST_HOST_URL) + API_V4_PROJECTS + projectId;
        ResponseEntity<AccessResponse> response1 = restTemplate.build().exchange(serviceUrl, HttpMethod.DELETE, requestEntity,
                AccessResponse.class);
        AccessResponse accessResponse = response1.getBody();
        //System.out.println("************************ " +accessResponse.getName());
        return accessResponse;

    }


    private static String encodeURLComponent(final String s) {
        if (s == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();

        try {
            for (int i = 0; i < s.length(); i++) {
                final char c = s.charAt(i);

                if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) ||
                        ((c >= '0') && (c <= '9')) ||
                        (c == '-') || (c == '.') || (c == '_') || (c == '~')) {
                    sb.append(c);
                } else {
                    final byte[] bytes = ("" + c).getBytes("UTF-8");

                    for (byte b : bytes) {
                        sb.append('%');

                        int upper = (((int) b) >> 4) & 0xf;
                        sb.append(Integer.toHexString(upper).toUpperCase(Locale.US));

                        int lower = ((int) b) & 0xf;
                        sb.append(Integer.toHexString(lower).toUpperCase(Locale.US));
                    }
                }
            }

            return sb.toString();
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("UTF-8 unsupported!?", uee);
        }
    }


    private CreateApplicationRequest createJsonProject(String applicationName, GitLabApi gitLabAdminApi, String updatedJson, String userName,
                                                       Integer userId, String privateToken, HttpHeaders httpHeaders) throws GitLabApiException {

        HttpEntity<String> requestEntity = new HttpEntity<>(httpHeaders);
        RestTemplateBuilder restTemplate = new RestTemplateBuilder();

        applicationName = applicationName + JSON_APPLICATON;
        applicationName =StringUtils.deleteWhitespace(applicationName);

        CreateApplicationRequest createApplicationRequest = new CreateApplicationRequest();
        Project project = new Project().withName(applicationName).withDescription(applicationName.toUpperCase())
                .withIssuesEnabled(true).withMergeRequestsEnabled(true).withWikiEnabled(true)
                .withSnippetsEnabled(true).withInitializeWithReadme(true).withVisibility(Visibility.PRIVATE);
        project.setRequestAccessEnabled(true);
        Project jsonProject = null;
        try {
            jsonProject = gitLabAdminApi.getProjectApi().createProject(project);

        } catch (GitLabApiException e) {
            createApplicationRequest.setErrorMsg(e.getMessage());
            return createApplicationRequest;
        }
        // adding the branch
        gitLabAdminApi.getRepositoryApi().createBranch(jsonProject.getId(), DEVELOPMENT_BRANCH_NAME, MASTER);
        gitLabAdminApi.getRepositoryApi().createBranch(jsonProject.getId(), PRISM_BRANCH_NAME, MASTER);


        createApplicationRequest.setProjectId(jsonProject.getId());
        CommitAction commitAction = new CommitAction().withAction(CommitAction.Action.CREATE).withContent(updatedJson)
                .withFilePath(SLASH + jsonProject.getName().toLowerCase() + JSON);
        System.out.println("Project ID :::: " + jsonProject.getId().toString());


        Commit commit = null;
        try {
            commit = gitLabAdminApi.getCommitsApi().createCommit(jsonProject.getId(), MASTER, INITIAL_COMMIT,
                    MASTER, null, null, Arrays.asList(commitAction));
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }

        log.info(commit.getMessage());

        String serviceUrl = TEST_HOST_URL + API_V4_PROJECTS + jsonProject.getId() + PROTECTED_BRANCHES_MASTER;

        ResponseEntity<String> response = restTemplate.build().exchange(serviceUrl, HttpMethod.DELETE,
                requestEntity, String.class);
        log.info(response.toString());
        if (!StringUtils.equals(ADMIN_TOKEN, privateToken)) {
            try {
                gitLabAdminApi.getProjectApi().addMember(jsonProject,userId,AccessLevel.DEVELOPER);

            } catch (GitLabApiException e) {
                e.printStackTrace();
            }
        }

        if (StringUtils.isBlank(createApplicationRequest.getErrorMsg())) {
            createApplicationRequest.setProjectId(jsonProject.getId());
        }

        log.info("Clonning the Json Project >> " + jsonProject.getName());
        try {
            getGitProject(userName, jsonProject, privateToken,null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Response from json Project" + createApplicationRequest.getProjectId());
        return createApplicationRequest;

    }


    private String createJavaProject(String json, String projectLocation, GitLabApi gitLabAdminApi, String privateToken, Integer userId, HttpHeaders headers,String userName) {


        // Generate the project using the JSON
        // Create a project out from the JSON Java Application
        try {
            String commandName = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD : LINUX_SH;
            String cmdDirectory = os.toLowerCase().startsWith(WINDOWS) ? WIN_CMD_DIR : LINUX_CMD_DIR;
            String projectBasePath = os.toLowerCase().startsWith(WINDOWS) ?WINDOWS_PRJ_GENERATED_DIR:LINUX_PRJ_GENERATED_DIR;
            String shellScriptLocation = os.toLowerCase().startsWith("windows") ? WINDOWS_BATCH_LOCATION : LINUX_BATCH_LOCATION;
            net.sf.json.JSONObject jsonApp = (net.sf.json.JSONObject) JSONSerializer.toJSON(json);
            net.sf.json.JSONObject application = (net.sf.json.JSONObject) jsonApp.get("Application");
            String javaApplicationName = (String) application.get("Name");
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            RestTemplateBuilder restTemplate = new RestTemplateBuilder();

            javaApplicationName = javaApplicationName + JAVA_APPLICATON;

            javaApplicationName = StringUtils.deleteWhitespace(javaApplicationName);

            String comments = "First-commit";
            String projectName = StringUtils.substringAfterLast(projectLocation, File.separator);
System.out.println("Inside-Commit  1>>>>>>>>>" + projectName);

            projectName = StringUtils.substringBeforeLast(projectName, ".");
System.out.println("Inside-Commit  2>>>>>>>>>" + projectName);

            projectName = StringUtils.deleteWhitespace(projectName);

            //customize the url
            System.out.println("Inside-Commit 3 >>>>>>>>>" + projectName);
            String stringBuilder = PROTOCOL + ADMIN_USER_NAME + ":" + encodeURLComponent(ADMIN_USER_PASSWORD) + "@" + GIT_LAB_URL + "/" +
                    ADMIN_USER_NAME + "/" + javaApplicationName + ".git";

            log.info(">>>>>>>>>>>>>> Project Commit Url >>>>" +stringBuilder);

            createOrUpdateGit(commandName, cmdDirectory, shellScriptLocation, projectBasePath + "/" + projectName, stringBuilder, comments,projectName);
            List<Project> maintainerProjects = gitLabAdminApi.getProjectApi().getMemberProjects();

            Project project1 = null;
            for (Project project2 : maintainerProjects) {
                if (javaApplicationName.equalsIgnoreCase(project2.getName())) {
                    project1 = project2;
                    break;
                }
            }

            if (project1 != null) {
                if (!StringUtils.equals(ADMIN_TOKEN, privateToken)) {
                    log.info("Requesting access for the Java project");
                    gitLabAdminApi.getProjectApi().addMember(project1,userId,AccessLevel.DEVELOPER);
                    System.out.println(">>>>>>Members Of this project now >>>"+gitLabAdminApi.getProjectApi().getMembers(project1.getId()));;
                }

                String javaServiceUrl = TEST_HOST_URL + API_V4_PROJECTS + project1.getId() + PROTECTED_BRANCHES_MASTER;
                restTemplate.build().exchange(javaServiceUrl, HttpMethod.DELETE, requestEntity, String.class);


                if (projectLocation != null) {
                    FileUtils.deleteQuietly(new File(projectLocation));
                }
                project1.setVisibility(Visibility.PRIVATE);
                gitLabAdminApi.getProjectApi().updateProject(project1);
            }

            try {

                String baseUrl = projectBasePath+userName+SLASH+userName+SLASH;
                System.out.println("project>>>>>> "+project1.getName() +"projectBasePath >?>>>>????????>>>>>>>"+baseUrl);
                getGitProject(userName, project1, privateToken,baseUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (Exception e) {

            e.printStackTrace();
            return "failure";
        }
        return "success";

    }

    @PostMapping("/enableRunner")
    @ApiOperation(value = "Enable runner for the project", hidden = true)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Enable runner for the project successfully", response = String.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),

    })
    public void enableRunner(String projectId,Integer runnerId)
    {
        GitLabApi gitLabAdminApi = new GitLabApi(ApiVersion.V4, TEST_HOST_URL, ADMIN_TOKEN);
        RunnersApi runnersApi = new RunnersApi(gitLabAdminApi);
        try {
            runnersApi.enableRunner(projectId,runnerId);
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }

    }


    private boolean sendMail(String to, String adminUserName, String userName, Project project) throws Exception {

        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            if (StringUtils.isNotBlank(to)) {
                if (to.contains(",")) {
                    mimeMessageHelper.setTo(to.split(","));
                } else {
                    mimeMessageHelper.setTo(to);
                }
            }

            Map<String, Object> templateDate = new HashMap<>();
            templateDate.put("projectUrl", project.getHttpUrlToRepo());
            templateDate.put("projectName", project.getName());
            templateDate.put("username", userName);
            templateDate.put("admin", adminUserName);
            Template processCode = freemarker.getTemplate("RequestAccess.ftl");
            String content = FreeMarkerTemplateUtils.processTemplateIntoString(processCode, templateDate);

            mimeMessageHelper.setReplyTo(from);
            mimeMessageHelper.setFrom(from);
            mimeMessageHelper.setSubject(REQUEST_ACCESS_SUBJECT);
            mimeMessageHelper.setText(content, true);
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;

        }
        return true;
    }

    @GetMapping("/checkMongoDBStatus")
@ApiResponses(value = {@ApiResponse(code = 200, message = "Success|OK"),
        @ApiResponse(code = 401, message = "not authorized!"),
        @ApiResponse(code = 403, message = "forbidden!!!"),
        @ApiResponse(code = 404, message = "not found!!!")})
@ApiOperation(value = Constants.GET_MY_APPLICATIONS_TXT ,hidden = true)
public String checkMongoDb() throws UnknownHostException {
    MongoClient mongoClient = new MongoClient("15.10.0.196", 27017);
    List<String> databases = mongoClient.getDatabaseNames();

    for (String dbName : databases) {
        if (dbName.equals("prism-test")) {
            log.info(">>>>> Successfully logged in to MongoDB! - 200" );
            return  "Successfully logged in to MongoDB! - 200";

        }

    }

    mongoClient.close();
   

    return "Error Connecting Database";

}

	


}
