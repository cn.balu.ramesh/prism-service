package com.prism.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.prism.constants.Constants;
import com.prism.form.PrismFieldNamingStrategy;
import com.prism.form.PrismForm;
import com.prism.form.Top;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeContorller {

	String getURL = null;

	public static <T> Object GsonToObjectConverter(String str, Class<T> clazz) {
		final GsonBuilder builder = new GsonBuilder();

		builder.setPrettyPrinting();
		builder.setFieldNamingStrategy(new PrismFieldNamingStrategy());

		Gson gson = builder.create();
		T json = gson.fromJson(str, clazz);
		return json;
	}

	public static String ObjectToGsonConverter(Object obj) {
		final GsonBuilder builder = new GsonBuilder();

		builder.setPrettyPrinting();
		builder.setFieldNamingStrategy(new PrismFieldNamingStrategy());

		Gson gson = builder.create();
		String json = gson.toJson(obj);
		return json;
	}

	@GetMapping(com.prism.constants.Constants.IN_FORM)
	public String inputForm(Model model) {

		List<String> projectTypeList = new ArrayList<>();
		projectTypeList.add("PrismServiceApplication");
		model.addAttribute("form", new PrismForm());
		model.addAttribute("projectType", projectTypeList);
		return "prism";
	}

	@PostMapping(com.prism.constants.Constants.GET_RESPONSE)
	public void getResponse(ModelMap modelMap, @ModelAttribute("form") PrismForm form,
			@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView) throws IOException, ValidationException {
		String os = SystemUtils.OS_NAME;
		ClassPathResource resource = new ClassPathResource(
				org.apache.commons.lang3.StringUtils.startsWithAny(os, "windows", "Windows")
						|| org.apache.commons.lang3.StringUtils.equalsIgnoreCase(os, "windows")
								? "json-schema-draft-06.json"
								: "classpath:json-schema-draft-06.json");

		Path path = Paths.get(file.getOriginalFilename());

		try {
			InputStream in = resource.getInputStream();
			//JSONObject jsonSchema = new JSONObject(in);
			JSONObject jsonSchema = new JSONObject(new JSONTokener(in));
			byte[] bytes = file.getBytes();

			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

			String line = null;

			FileReader fileReader = new FileReader(file.getOriginalFilename());
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			StringBuilder builder = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				builder.append(line);
				//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> " + line);
			}
			bufferedReader.close();

			String processStr = builder.toString();
			JSONObject jsonSubject = new JSONObject(new JSONTokener(processStr));
			Schema schema = SchemaLoader.load(jsonSchema);
			schema.validate(jsonSubject);
			Top app = (Top) GsonToObjectConverter(processStr, Top.class);
			String fileName = app.getApplication().getName();

			HttpHeaders headers = new HttpHeaders();

			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			// Request to return JSON format
			headers.setContentType(MediaType.APPLICATION_JSON);

			// HttpEntity<String>: To get result as String.
			HttpEntity<String> entity = new HttpEntity<String>(processStr, headers);

			// RestTemplate
			RestTemplateBuilder restTemplate = new RestTemplateBuilder();

			// Send request with GET method, and Headers.
			//System.out.println("file is before");

			ResponseEntity<byte[]> response1 = restTemplate.build().exchange(Constants.URL_CREATE_APPLICATION,
					HttpMethod.POST, entity, byte[].class);

			restTemplate.build();

		/*	ResponseEntity<ValidationResponse> response2 = restTemplate.build().exchange(Constants.URL_CREATE_APPLICATION,
					HttpMethod.POST, entity, ValidationResponse.class);
					List<ValidationException>errorResponse = response2.getBody().getResponses();*/
				/*if (errorResponse!=null && errorResponse.size()>0){
					throw new RuntimeException("Error");
				}*/



			response.setContentType("application/octet-stream");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileName + ".zip");

			try {
				//Files.write(path, response2.getBody().getFile());
			  Files.write(path, response1.getBody());
				Files.copy(path, response.getOutputStream());
				FileUtils.deleteQuietly(path.toFile());

			} catch (Exception ex) {
				if (ex instanceof ValidationException) {
					List<String> errors = new ArrayList<>();
					errors.add("JSON File Error");
					errors.add("********************************************************************************************");
					var error = ((ValidationException) ex).getCausingExceptions().stream()
							.flatMap(e1 -> e1.getAllMessages().stream()).collect(Collectors.toList());
                    var allError = ((ValidationException) ex).getAllMessages();
					PrintWriter out = response.getWriter();
					errors.addAll(error);
                    errors.addAll(allError);
					errors.forEach(out::println);
					FileUtils.deleteQuietly(path.toFile());
				}
			}

		} catch (Exception e) {
          //  System.out.println(e);
			if (e instanceof ValidationException) {
				List<String> errors = new ArrayList<>();
				errors.add("JSON File Error");
				errors.add(
						"********************************************************************************************");
				var error = ((ValidationException) e).getCausingExceptions().stream()
						.flatMap(e1 -> e1.getAllMessages().stream()).collect(Collectors.toList());
                var allError = ((ValidationException) e).getAllMessages();
				PrintWriter out = response.getWriter();
				errors.addAll(error);
				errors.addAll(allError);
				errors.forEach(out::println);
				FileUtils.deleteQuietly(path.toFile());
			}

		}

	}

	@PostMapping("/upload") // //new annotation since 4.3
	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:uploadStatus";
		}
		try {

			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			Path path = Paths.get(file.getOriginalFilename());
			Files.write(path, bytes);

			redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + file.getOriginalFilename() + "'");

		} catch (IOException e) {
			e.printStackTrace();
		}

		return "redirect:/uploadStatus";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		return "uploadStatus";
	}

	@GetMapping("/prismerror")
	public String error() {
		return "prismerror";
	}

}
