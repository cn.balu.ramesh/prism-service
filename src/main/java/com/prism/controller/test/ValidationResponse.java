package com.prism.controller.test;

import java.util.List;

import org.everit.json.schema.ValidationException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;
@ToString 
@JsonIgnoreProperties(value= {"exception"})
public class ValidationResponse  {

	 @JsonCreator
    public ValidationResponse() {
		
	}


	@JsonProperty("file")
    private byte[] file;


    @JsonProperty("message")
    private String message;


    @JsonProperty("responses")
    List<ValidationException> responses;


    public byte[] getFile() {
        return file;
    }

    public String getMessage() {
        return message;
    }

    public List<ValidationException> getResponses() {
        return responses;
    }


   
    public void setResponses( @JsonProperty("responses")List<ValidationException> responses) {
        this.responses = responses;
    }
}