/**
 * 
 */
package com.prismapp.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Lenovo
 *
 */
public class ExecuteShellScript {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			//ProcessBuilder processBuilder = new ProcessBuilder();
			// Run a shell command
			Process process = Runtime.getRuntime().exec("sudo chmod -R 775 /opt/gitlabprj/");
			
			StringBuilder output = new StringBuilder();

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));
			
			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
