package com.prismapp.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Value;

public class ShellTesting {
	  
	   @Value("${WINDOWS_BATCH_LOCATION}") private static String WINDOWS_BATCH_LOCATION ;
	   @Value("${LINUX_BATCH_LOCATION}") private static String LINUX_BATCH_LOCATION ;
	   
	   @Value("${LINUX_PRJ_GENERATED_DIR}") private static String LINUX_PRJ_GENERATED_DIR ;
	
	public static void main(String[] args) {
		  
        String os = SystemUtils.OS_NAME;
        String commandName = os.toLowerCase().startsWith("windows") ? "cmd" : "sh";
        String cmdDirectory = os.toLowerCase().startsWith("windows") ? "/c" : "-c";
        String projectBasePath =   LINUX_PRJ_GENERATED_DIR;
        
        
        String shellScriptLocation = os.toLowerCase().startsWith("windows") ? WINDOWS_BATCH_LOCATION : LINUX_BATCH_LOCATION;
        String projectLocation = projectBasePath + File.separator + "testing";
        String gitURL = "https://prism.admin:prim$123@gitlab.com/prism.admin/testing.git";
        String comments = "First commit";
        createOrUpdateGit(commandName,cmdDirectory,shellScriptLocation,projectLocation, gitURL,comments);
        
        
	}
	
	private static void createOrUpdateGit(String commandName,String command ,String shellScriptLocation ,String projectLocation ,String gitURL ,String comments){
        
		
		
		try {
            System.out.println("THE BASE PATH IS ******************"+projectLocation);
            
            System.out.println("THE gitURL IS ******************"+gitURL);
            String[] cmd = { commandName,command , shellScriptLocation+" " +projectLocation+ " " +  gitURL+ " " +  comments};
            System.out.println(Arrays.toString(cmd));
            ProcessBuilder pb = new ProcessBuilder(cmd);
            pb.redirectErrorStream(true);
            Process p = pb.start();

            p.waitFor();
            BufferedReader reader=new BufferedReader(new InputStreamReader( p.getInputStream()));
            String line;

            do {
                line = reader.readLine();
                System.out.println(line);
            } while (line != null);


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
